<?php

function colpt_menu_page_render(){
		
		?>
		<table>
			<tr>
				<td>
					<img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/icon32.png'; ?>" />
				</td>
				<td>
					<h1>College People Tools Options Page</h1>
				</td>
			</tr>
		</table>
		
		
		<div class=wrap>
			<form method="post" action="options.php">
				<?php settings_fields( 'colpt_people_tools_options_page' ); ?>
				<?php submit_button(); ?>
				<?php do_settings_sections ( 'colpt_people_tools_options_page' ); ?>
				<?php submit_button(); ?>
				<?php do_settings_sections ( 'colpt_page_options_section' ); ?>
			</form>
		</div>
			
		<form method="post" action="<?php echo plugin_dir_url( __FILE__ ) . 'misc_tools.php';?>">
			<input type="submit" value="Setup WP User Avatar" name="submit"> <!-- assign a name for the button -->
		</form>


		
		<?php
		
}


//Register Options Sections

function colpt_fields_api_init() {
	add_settings_section(
		'colpt_tabbed_fields_section',
		'People Editing Area Options',
		'colpt_tabbed_fields_section_callback_function',
		'colpt_people_tools_options_page'
	);
	add_settings_section(
		'colpt_pp_fields_section',
		'The People Page Layout',
		'colpt_pp_fields_section_callback_function',
		'colpt_people_tools_options_page'
	);
	add_settings_section(
		'colpt_page_options_section',
		'Pages Options',
		'colpt_page_options_section_callback_function',
		'colpt_people_tools_options_page'
	);
	

	//Profile Page Options
	
	register_setting( 'colpt_people_tools_options_page', 'colpt_bio_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_cv_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_courses_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_publications_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_research_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customtab1_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customtab1_name_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customtab2_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customtab2_name_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customshortfield1_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customshortfield1_name_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customshortfield2_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_customshortfield2_name_field_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_profile_page_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_people_page_selection' );
	
	//People Page Layout
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_top_text_editor' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_layout_piece_top_text_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_layout_piece_1_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_layout_piece_1_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_layout_piece_2_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_layout_piece_2_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_name_and_title_layout_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_name_type_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_1_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_1_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_1_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_2_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_2_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_2_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_3_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_3_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_3_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_4_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_4_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_4_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_5_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_5_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_5_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_6_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_6_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_6_active_setting' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_7_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_7_linkon_selection' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_pp_block_7_active_setting' );
	
	register_setting( 'colpt_people_tools_options_page', 'colpt_people_page_okay' );
	register_setting( 'colpt_people_tools_options_page', 'colpt_profile_page_okay' );
	
	
	
	
	
	
	add_settings_field(
		'colpt_bio_field_setting',
		'Show Bio Tab',
		'colpt_bio_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_cv_field_setting',
		'Show CV Tab',
		'colpt_cv_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_courses_field_setting',
		'Show Courses Tab',
		'colpt_courses_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_publications_field_setting',
		'Show Publications Tab',
		'colpt_publications_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_research_field_setting',
		'Show Research Tab',
		'colpt_research_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_customtab1_name_field_setting',
		'Show Custom Tab 1',
		'colpt_customtab1_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_customtab2_name_field_setting',
		'Show Custom Tab 2',
		'colpt_customtab2_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_customshortfield1_name_field_setting',
		'Show Custom Short Field 1',
		'colpt_customshortfield1_name_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
		'colpt_customshortfield2_name_field_setting',
		'Show Custom Short Field 2',
		'colpt_customshortfield2_name_field_setting_callback_function',
		'colpt_people_tools_options_page',
		'colpt_tabbed_fields_section'
	);
	add_settings_field(
 		'colpt_profile_page_selection',
 		'Profile Page',
 		'colpt_profile_page_selection_callback_function',
 		'colpt_people_tools_options_page',
 		'colpt_page_options_section'
 	);
 	add_settings_field(
 		'colpt_people_page_selection',
 		'People Page',
 		'colpt_people_page_selection_callback_function',
 		'colpt_people_tools_options_page',
 		'colpt_page_options_section'
 	);
	add_settings_field(
		'colpt_pp_name_and_title_layout_selection',
		'Name and Title Layout',
		'colpt_pp_name_and_title_layout_selection_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
	add_settings_field(
		'colpt_pp_name_type_selection',
		'Name Format',
		'colpt_pp_name_type_selection_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
	add_settings_field(
		'colpt_pp_block_6_selection',
		'Extra Column 1',
		'colpt_pp_block_6_selection_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
	add_settings_field(
		'colpt_pp_block_7_selection',
		'Extra Column 2',
		'colpt_pp_block_7_selection_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
	add_settings_field(
		'colpt_pp_layout_pieces',
		'Layout of People Page',
		'colpt_pp_layout_pieces_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
	add_settings_field(
		'colpt_pp_top_text_editor',
		'Top of Page Content',
		'colpt_pp_top_text_editor_callback_function',
		'colpt_people_tools_options_page',
		'colpt_pp_fields_section'
	);
}


add_action( 'admin_init', 'colpt_fields_api_init' );

// Sections Callback Functions
	
function colpt_tabbed_fields_section_callback_function() {
  
}

function colpt_expansion_fields_section_callback_function() {
  
}

function colpt_page_options_section_callback_function() {
	
}

function colpt_pp_fields_section_callback_function() {
	echo '<div class="colpt-bulk-text">What you see in this preview may not reflect colors, styles, and actual alignment of your people Page. Please view the actual page to check the design. Click here to view<br />&nbsp;</div>';
	
	echo '<div class="admin_preview preview-wrapper">
	<div class="pp-header-block"> <!--Start Header Block-->
			<div class="colpt_pp_header">Name</div>';
			
	if (get_option('colpt_pp_name_and_title_layout_selection') !== '3') { echo '<div class="colpt_pp_header">Title</div>'; }
			
	echo	'<div class="colpt_pp_header">Email</div>
			<div class="colpt_pp_header">Phone</div>
			<div class="colpt_pp_header">Office</div>';
		
	if (get_option('colpt_pp_block_6_selection') !== 1 ) { echo '<div class="colpt_pp_header">Extra Column</div>'; }
	
	echo	'</div> <!--End Header Block-->
			<div class="person-block"> <!--Start Person Block 1-->';
			
	if (get_option('colpt_pp_name_and_title_layout_selection') !== '2') { echo '<div class="wrap_it prop-2">'; } 
	
	echo	'<div class="show_it prop-2"><a href="http://localhost/test4/index.php/profile-page/?user=1">Tyler Pruitt</a></div>
			<div class="show_it prop-2">Instructional Tech Specialist</div>';
	
	if (get_option('colpt_pp_name_and_title_layout_selection') !== '2') { echo '</div>'; }	
	
	echo	'<div class="show_it prop-2"><a href="mailto:pruitttr@wfu.edu">pruitttr@wfu.edu</a></div>
			<div class="show_it prop-2"></div>
			<div class="show_it prop-2">Tribble C2A</div>';
		
	if (get_option('colpt_pp_block_6_selection') !== 1 ) { echo '<div class="show_it prop-2">Extra Column Info</div>'; }
	
	echo	'</div> <!--End Person Block 1-->
			</div> <!-- End Preview Wrapper -->';
}

// Tabbed Fields Callback Functions

function colpt_bio_field_setting_callback_function() {
     
    echo '<input type="checkbox" id="colpt_bio_field_setting" name="colpt_bio_field_setting" value="1"' . checked( 1, get_option( 'colpt_bio_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in their "Bio" Tab via their People Editing Area.</label>';
}

function colpt_cv_field_setting_callback_function() {
     
    echo '<input type="checkbox" id="colpt_cv_field_setting" name="colpt_cv_field_setting" value="1"' . checked( 1, get_option( 'colpt_cv_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in their "CV" Tab via their People Editing Area.</label>';
}

function colpt_courses_field_setting_callback_function() {
     
    echo '<input type="checkbox" id="colpt_courses_field_setting" name="colpt_courses_field_setting" value="1"' . checked( 1, get_option( 'colpt_courses_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in their "Courses" Tab via their People Editing Area.</label>';
}

function colpt_publications_field_setting_callback_function() {
     
    echo '<input type="checkbox" id="colpt_publications_field_setting" name="colpt_publications_field_setting" value="1"' . checked( 1, get_option( 'colpt_publications_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in their "Publications" Tab via their People Editing Area.</label>';
}

function colpt_research_field_setting_callback_function() {
     
    echo '<input type="checkbox" id="colpt_research_field_setting" name="colpt_research_field_setting" value="1"' . checked( 1, get_option( 'colpt_research_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in their "Research" Tab via their People Editing Area.</label>';
}

function colpt_customtab1_field_setting_callback_function() {

    echo '<input type="text" id="colpt_customtab1_name_field_setting" name="colpt_customtab1_name_field_setting" value="' . get_option( 'colpt_customtab1_name_field_setting' ) . '" />';
	echo '<input type="checkbox" id="colpt_customtab1_field_setting" name="colpt_customtab1_field_setting" value="1"' . checked( 1, get_option( 'colpt_customtab1_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in the Custom Tab you have defined to the left via their People Editing Area.</label>';
	echo '<div class="label1">Enter into the box above the name your would like for your Custom Tab</div>';
}

function colpt_customtab2_field_setting_callback_function() {

    echo '<input type="text" id="colpt_customtab2_name_field_setting" name="colpt_customtab2_name_field_setting" value="' . get_option( 'colpt_customtab2_name_field_setting' ) . '" />';
	echo '<input type="checkbox" id="colpt_customtab2_field_setting" name="colpt_customtab2_field_setting" value="1"' . checked( 1, get_option( 'colpt_customtab2_field_setting' ), false ) . ' />'; 
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in the Custom Tab you have defined to the left via their People Editing Area.</label>';
	echo '<div class="label1">Enter into the box above the name your would like for your Custom Tab</div>';
}

function colpt_customshortfield1_name_field_setting_callback_function() {

    echo '<input type="text" id="colpt_customshortfield1_name_field_setting" name="colpt_customshortfield1_name_field_setting" value="' . get_option( 'colpt_customshortfield1_name_field_setting' ) . '" />';
	echo '<input type="checkbox" id="colpt_customshortfield1_field_setting" name="colpt_customshortfield1_field_setting" value="1"' . checked( 1, get_option( 'colpt_customshortfield1_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in the Custom Short Field you have defined to the left via their People Editing Area. This information may be used in a column on the People Page.</label>';
	echo '<div class="label1">Enter into the box above the name your would like for your Custom Short Field</div>';
}

function colpt_customshortfield2_name_field_setting_callback_function() {

    echo '<input type="text" id="colpt_customshortfield2_name_field_setting" name="colpt_customshortfield2_name_field_setting" value="' . get_option( 'colpt_customshortfield2_name_field_setting' ) . '" />';
	echo '<input type="checkbox" id="colpt_customshortfield2_field_setting" name="colpt_customshortfield2_field_setting" value="1"' . checked( 1, get_option( 'colpt_customshortfield2_field_setting' ), false ) . ' />';
	echo '<label class="label2" for="colpt_bio_field_setting">Checking this box will allow users to fill in the Custom Short Field you have defined to the left via their People Editing Area. This information may be used in a column on the People Page.</label>';
	echo '<div class="label1">Enter into the box above the name your would like for your Custom Short Field</div>';
}


function colpt_profile_page_selection_callback_function() {
	
	
	$colpt_profile_page_selection_current_value = get_option( 'colpt_profile_page_selection' );
	$colpt_profile_page_selection_args = array(
		'depth'                 => 0,
		'child_of'              => 0,
		'selected'              => $colpt_profile_page_selection_current_value,
		'echo'                  => 1,
		'name'                  => 'colpt_profile_page_selection',
		'id'                    => 'colpt_profile_page_selection',
		'class'                 => 'You must select a page',
		'show_option_none'      => null, // string
		'show_option_no_change' => null, // string
		'option_none_value'     => null, // string
	);
	wp_dropdown_pages( $colpt_profile_page_selection_args );
	?>
	<div class="label1">Select a page you've already created</div>
	
	
<?php } 

function colpt_people_page_selection_callback_function() {
	$colpt_people_page_selection_current_value = get_option( 'colpt_people_page_selection' );
	$colpt_people_page_selection_args = array(
		'depth'                 => 0,
		'child_of'              => 0,
		'selected'              => $colpt_people_page_selection_current_value,
		'echo'                  => 1,
		'name'                  => 'colpt_people_page_selection',
		'id'                    => 'colpt_people_page_selection',
		'class'                 => 'You must select a page',
		'show_option_none'      => null, // string
		'show_option_no_change' => null, // string
		'option_none_value'     => null, // string
	);
	wp_dropdown_pages( $colpt_people_page_selection_args );
	
	update_option( 'colpt_pp_block_2_selection', 4 );
	update_option( 'colpt_pp_block_3_selection', 5 );
	update_option( 'colpt_pp_block_4_selection', 6 );
	update_option( 'colpt_pp_block_5_selection', 7 );
}

// People Page Callback Functions


function colpt_pp_layout_pieces_callback_function() {
	?>
	<div class="colpt_pp_layout_pieces_options_wrapper">
			<div class="colpt_pp_layout_piece_options_wrapper">
					<div class="colpt_pp_layout_piece_setting">
						<input type="checkbox" id="colpt_pp_layout_piece_top_text_setting" name="colpt_pp_layout_piece_top_text_setting" value="1" <?php checked( 1, get_option( 'colpt_pp_layout_piece_top_text_setting' ), true ) ?>/>
						</div> <!-- End PP Layout Piece Option -->
					<div class="colpt_pp_layout_piece_wording">
						Text at top of page
						</div> <!-- End PP Layout Piece Wording -->
				</div> <!-- End PP Layout Piece Options Wrapper -->
			<div class="colpt_pp_layout_piece_options_wrapper">
					<div class="colpt_pp_layout_piece_setting">
						<input type="checkbox" id="colpt_pp_layout_piece_1_setting" name="colpt_pp_layout_piece_1_setting" value="1" <?php checked( 1, get_option( 'colpt_pp_layout_piece_1_setting' ), true ) ?>/>
						</div> <!-- End PP Layout Piece Option -->
					<div class="colpt_pp_layout_piece_wording">
						Faculty Listing
						</div> <!-- End PP Layout Piece Wording -->
				</div> <!-- End PP Layout Piece Options Wrapper -->
			<div class="colpt_pp_layout_piece_options_wrapper">
					<div class="colpt_pp_layout_piece_setting">
						<input type="checkbox" id="colpt_pp_layout_piece_2_setting" name="colpt_pp_layout_piece_2_setting" value="1" <?php checked( 1, get_option( 'colpt_pp_layout_piece_2_setting' ), true ) ?>/>
						</div> <!-- End PP Layout Piece Option -->
					<div class="colpt_pp_layout_piece_wording">
						Staff Listing
						</div> <!-- End PP Layout Piece Wording -->
				</div> <!-- End PP Layout Piece Options Wrapper -->
	
	
	
		</div> <!-- End PP Layout Pieces Options Wrapper -->
	<?php
}


function colpt_ap_block_display($option) {
echo '<select id="' . $option . '" name="' . $option . '">';
		echo '<option value="1"';
			if(get_option($option) == '1') { echo 'selected="selected"'; }
			echo '>None (Do Not Display)</option>';
		echo '<option value="2"';
			if(get_option($option) == '2') { echo 'selected="selected"'; }
			echo '>Full Name (First Last)</option>';
		echo '<option value="3"';
			if(get_option($option) == '3') { echo 'selected="selected"'; }
			echo '>Comma Name (Last, First)</option>';
		echo '<option value="4"';
			if(get_option($option) == '4') { echo 'selected="selected"'; }
			echo '>Title</option>';
		echo '<option value="5"';
			if(get_option($option) == '5') { echo 'selected="selected"'; }
			echo '>Email</option>';
		echo '<option value="6"';
			if(get_option($option) == '6') { echo 'selected="selected"'; }
			echo '>Phone</option>';
		echo '<option value="7"';
			if(get_option($option) == '7') { echo 'selected="selected"'; }
			echo '>Office</option>';
		echo '<option value="8"';
			if(get_option($option) == '8') { echo 'selected="selected"'; }
			echo '>';
			echo get_option('colpt_customshortfield1_name_field_setting');
			echo '</option>';
		echo '<option value="9"';
			if(get_option($option) == '9') { echo 'selected="selected"'; }
			echo '>';
			echo get_option('colpt_customshortfield2_name_field_setting');
			echo '</option>';
	echo '</select>';
}



function colpt_ap_block_linkon_display($option) {
	echo '<select id="' . $option . '" name="' . $option . '">';
		echo '<option value="1"';
			if(get_option($option) == '1') { 
			echo 'selected="selected"'; 
			}
			echo '>None</option>';
		echo '<option value="email"';
			if(get_option($option) == 'email') { 
			echo 'selected="selected"'; 
			}
			echo '>Email</option>';
		echo '<option value="profile"';
			if(get_option($option) == 'profile') { 
			echo 'selected="selected"'; 
			}
			echo '>Profile Link</option>';
	echo '</select>';
}

function colpt_ap_block_active_display($option) {
	echo '<input type="checkbox" id="' . $option . '" name="' . $option . '" value="1"' . checked( 1, get_option( $option ), false ) . ' />';
}



function colpt_pp_name_and_title_layout_selection_callback_function() {
	echo '<select id="colpt_pp_name_and_title_layout_selection" name="colpt_pp_name_and_title_layout_selection">';
		echo '<option value="2"';
			if(get_option('colpt_pp_name_and_title_layout_selection') == '2') { 
			echo 'selected="selected"'; 
			}
			echo '>Separate Columns</option>';
		echo '<option value="3"';
			if(get_option('colpt_pp_name_and_title_layout_selection') == '3') { 
			echo 'selected="selected"'; 
			}
			echo '>Name above Title</option>';
		echo '<option value="1"';
			if(get_option('colpt_pp_name_and_title_layout_selection') == '1') { 
			echo 'selected="selected"'; 
			}
			echo '>None</option>';
	echo '</select>';
}

function colpt_pp_name_type_selection_callback_function() {
	
	echo '<select id="colpt_pp_block_1_selection" name="colpt_pp_block_1_selection">';
		echo '<option value="2"';
			if(get_option('colpt_pp_block_1_selection') == '2') { 
			echo 'selected="selected"'; 
			}
			echo '>First Last (Full Name)</option>';
		echo '<option value="3"';
			if(get_option('colpt_pp_block_1_selection') == '3') { 
			echo 'selected="selected"'; 
			}
			echo '>Last, First (Comma Separated)</option>';
	echo '</select>';
}


function colpt_pp_block_6_selection_callback_function() {
    colpt_ap_block_display('colpt_pp_block_6_selection');
}

function colpt_pp_block_7_selection_callback_function() {
    colpt_ap_block_display('colpt_pp_block_7_selection');
}

function colpt_pp_top_text_editor_callback_function() {
    echo '<div class="options_editor_wrapper">';
    $colpt_pp_top_text_editor = get_option( 'colpt_pp_top_text_editor' );
	wp_editor( $colpt_pp_top_text_editor, 'colpt_pp_top_text_editor' );
	echo '</div>';
}