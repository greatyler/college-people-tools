<?php
/*
Plugin Name: College People Tools
Plugin URI: https://bitbucket.org/greatyler/college-people-tools
Description: Add Extensive User and People Management Tools for The WFU College Theme
Version: 0.6.1
Author: Tyler Pruitt, Robert Vidrine
Author URI: http://tpruitt.capeville.wfunet.wfu.edu/tylerpress/
Text Domain: college-people-tools
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Bitbucket Plugin URI: greatyler/college-people-tools
Bitbucket Branch: master

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.
This plugin is developed in conjunction with College Web Starter Theme by Robert Vidrine.
Thank You to Jo Lowe, Tommy Murphy, and Robert Vidrine for code contributions,
design consultation, and general sanity checks throughout the development process.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

College People is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

College Menus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/







// Base materials


function colpt_css_registration() {
wp_register_style('colpt', plugins_url('colpt.css',__FILE__ ));
wp_enqueue_style('colpt');
}
add_action( 'wp_enqueue_scripts','colpt_css_registration');

function colpt_admin_bar_render() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu('edit');
}

function colpt_page_templates_setup() {
	if ( is_page_template( 'people_page.php' ) || is_page_template( 'profile_page.php' ) ) {
		add_action( 'wp_before_admin_bar_render', 'colpt_admin_bar_render' );	
	}
}
add_action( 'wp_enqueue_scripts', 'colpt_page_templates_setup' );

include(dirname(__FILE__) . '/pagetemplater.php');
include(dirname(__FILE__) . '/help.php');
include(dirname(__FILE__) . '/menu_page.php');


function colpt_enqueue_admin_css() {
wp_register_style( 'colpt_admin_css', plugins_url('css/colpt_admin_css.css',__FILE__ ));
        wp_enqueue_style( 'colpt_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'colpt_enqueue_admin_css' );


function colpt_enqueue_colptcss_to_admin() {
wp_register_style( 'colpt_css_in_admin', plugins_url('colpt.css',__FILE__ ));
        wp_enqueue_style( 'colpt_css_in_admin' );
}

add_action( 'admin_enqueue_scripts', 'colpt_enqueue_colptcss_to_admin' );





/*
 * @author    Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright Copyright (c) 2011, Thomas Griffin
 * @license   GPL-2.0+
*/

require_once dirname(__FILE__) . '/tgm-plugin/colpt-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'colpt_register_required_plugins' );


function colpt_register_required_plugins() {
	
	$plugins = array(
	
		array(
				'name'        => 'Tabby Responsive Tabs',
				'slug'        => 'tabby-responsive-tabs',
				'is_callable' => 'tabby_init',
				'force_activation' => true,
			),
		array(
				'name'        => 'WP User Avatar',
				'slug'        => 'wp-user-avatar',
				'is_callable' => 'wp_user_avatar_init',
				'force_activation' => true,
			),	
			
	);
	
	$config = array(
		'id'           => 'colpt_tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'options-general.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		
		'strings'      => array(
			'page_title'                      => __( 'Plugins Required for College People Tools', 'theme-slug' ),
			'menu_title'                      => __( 'Install People Tools Plugins', 'theme-slug' ),
			'installing'                      => __( 'Installing Plugin: %s', 'theme-slug' ), // %s = plugin name.
			'oops'                            => __( 'Something went wrong with the plugin API.', 'theme-slug' ),
			'notice_can_install_required'     => _n_noop(
				'College People Tools requires the following plugin: %1$s.',
				'College People Tools requires the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_install_recommended'  => _n_noop(
				'College People Tools recommends the following plugin: %1$s.',
				'College People Tools recommends the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_install'           => _n_noop(
				'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with College People Tools: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with College People Tools: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_update'            => _n_noop(
				'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'notice_cannot_activate'          => _n_noop(
				'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
				'theme-slug'
			), // %1$s = plugin name(s).
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'theme-slug'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'theme-slug'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'theme-slug'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'theme-slug' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'theme-slug' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'theme-slug' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'theme-slug' ),  // %1$s = plugin name(s).
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for College People Tools. Please update the plugin.', 'theme-slug' ),  // %1$s = plugin name(s).
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'theme-slug' ), // %s = dashboard link.
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tgmpa' ),

			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		),
		
	);

	tgmpa( $plugins, $config );
}

























// Menu Options

function change_users_to_people() {
	global $menu;
	global $submenu;
	$menu[70][0] = 'People';
	$submenu['users.php'][5][0] = 'All People';
    $submenu['users.php'][10][0] = 'Add Person';
}

add_action( 'admin_menu', 'change_users_to_people' );






//Add Main Admin Menu Page
add_action ( 'admin_menu', 'colpt_menu_page');
function colpt_menu_page() {
		$colpt_menu_page = add_menu_page ( 'People Tools', 'People Tools', 'read', 'colptslug', 'colpt_menu_page_render', plugin_dir_url( __FILE__ ) . 'images/icon_cream.png' );
		add_action( 'load-' . $colpt_menu_page, 'load_colptcss_in_admin' );
		$colpt_help_page = add_submenu_page ( 'colptslug', 'People Tools Help', 'Help (People Tools)', 'remove_users', 'colpt_help_slug', 'colpt_help_render' );
		$colpt_people_settings_subpage = add_users_page ( 'Edudemia People Tools', 'Settings', 'remove_users', 'colpt_people_settings_subpage_slug', 'colpt_menu_page_render' );
	}
	function load_colptcss_in_admin(){
			// Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
			add_action( 'admin_enqueue_styles', 'enqueue_colptcss_in_admin' );
		}
	function enqueue_colptcss_in_admin(){
			wp_enqueue_script( 'colpt_css_in_admin', plugins_url('colpt.css',__FILE__ ) );
		}


//Add User-People Profile Fields


add_action( 'show_user_profile', 'show_tabbed_profile_fields' );
add_action( 'edit_user_profile', 'show_tabbed_profile_fields' );
 
function show_tabbed_profile_fields( $user ) { 


$colpt_customtab1_name = get_option('colpt_customtab1_name_field_setting');
$colpt_customtab2_name = get_option('colpt_customtab2_name_field_setting');
$colpt_customshortfield1_name = get_option('colpt_customshortfield1_name_field_setting');
$colpt_customshortfield2_name = get_option('colpt_customshortfield2_name_field_setting');

?>

    <h3>Faculty Profile Page Information</h3>
    <table class="form-table">
		<tr>
			<th><label>Member Type</label></th>
             <td>
				<select id="colpt_member_type" name="colpt_member_type">
				<option value="faculty" <?php if(get_the_author_meta('colpt_member_type', $user->ID) == 'faculty') { echo 'selected="selected"'; } ?>>Faculty</option>
				<option value="staff" <?php if(get_the_author_meta('colpt_member_type', $user->ID) == 'staff') { echo 'selected="selected"'; } ?>>Staff</option>
				<option value="other" <?php if(get_the_author_meta('colpt_member_type', $user->ID) == 'hidden') { echo 'selected="selected"'; } ?>>Hidden</option>
				</select>
                <span class="description">Select what category this person is. This will affect which pages this user's information will appaer on.</span>
            </td>
        </tr> 
		<?php if ( get_option( 'colpt_customshortfield1_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $colpt_customshortfield1_name; ?></label></th>
             <td>
                <input type="text" id="colpt_customshortfield1" name="colpt_customshortfield1" size="40" value="<?php echo get_the_author_meta( 'colpt_customshortfield1', $user->ID ); ?>">
                <span class="description">Please create your <?php echo $colpt_customshortfield1_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_customshortfield2_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $colpt_customshortfield2_name; ?></label></th>
             <td>
				<input type="text" id="colpt_customshortfield2" name="colpt_customshortfield2" size="40">
                <span class="description">Please fill in <?php echo $colpt_customshortfield2_name ?>.</span>
            </td>
        </tr>
		<?php } ?>
        <?php if ( get_option( 'colpt_bio_field_setting' ) == 1 ) { ?>
		<tr>
			<th><label>Bio</label></th>
             <td>
				<?php $colpt_bio = get_the_author_meta( 'colpt_bio', $user->ID );
				wp_editor( $colpt_bio, 'colpt_bio' ); ?>
                <br />
                <span class="description">Please create your Bio Section.</span>
            </td>
        </tr> 
		 <?php } ?>
		 <?php if ( get_option( 'colpt_cv_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>CV</label></th>
             <td>
                <?php $colpt_cv = get_the_author_meta( 'colpt_cv', $user->ID );
				wp_editor( $colpt_cv, 'colpt_cv' ); ?><br />
                <span class="description">Please create your CV Section.</span>
            </td>
        </tr>
		<tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_courses_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Courses</label></th>
             <td>
                <?php $colpt_courses = get_the_author_meta( 'colpt_courses', $user->ID );
				wp_editor( $colpt_courses, 'colpt_courses' ); ?><br />
                <span class="description">Please create your Courses Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_publications_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Publications</label></th>
             <td>
                <?php $colpt_publications = get_the_author_meta( 'colpt_publications', $user->ID );
				wp_editor( $colpt_publications, 'colpt_publications' ); ?><br />
                <span class="description">Please create your Publications Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_research_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label>Research</label></th>
             <td>
                <?php $colpt_research = get_the_author_meta( 'colpt_research', $user->ID );
				wp_editor( $colpt_research, 'colpt_research' ); ?><br />
                <span class="description">Please create your Research Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_customtab1_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $colpt_customtab1_name; ?></label></th>
             <td>
                <?php $colpt_customtab1 = get_the_author_meta( 'colpt_customtab1', $user->ID );
				wp_editor( $colpt_customtab1, 'colpt_customtab1' ); ?><br />
                <span class="description">Please create your <?php echo $colpt_customtab1_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		<?php if ( get_option( 'colpt_customtab2_field_setting' ) == 1 ) { ?>
		<tr>
            <th><label><?php echo $colpt_customtab2_name; ?></label></th>
             <td>
                <?php $colpt_customtab1 = get_the_author_meta( 'colpt_customtab2', $user->ID );
				wp_editor( $colpt_customtab2, 'colpt_customtab2' ); ?><br />
                <span class="description">Please create your <?php echo $colpt_customtab2_name ?> Section.</span>
            </td>
        </tr>
		<?php } ?>
		
    </table>
	
	
<?php }





function validate_wfu_phone () {
	//preg_replace(^1
}






 
function my_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
 
	$customtab1_metaname = (string)$customtab1_metaname;
	$customshortfield1_metaname = (string)$customshortfield1_metaname;
    update_user_meta( absint( $user_id ), 'colpt_bio', wp_kses_post( $_POST['colpt_bio'] ) );
	update_user_meta( absint( $user_id ), 'colpt_cv', wp_kses_post( $_POST['colpt_cv'] ) );
	update_user_meta( absint( $user_id ), 'colpt_courses', wp_kses_post( $_POST['colpt_courses'] ) );
	update_user_meta( absint( $user_id ), 'colpt_publications', wp_kses_post( $_POST['colpt_publications'] ) );
	update_user_meta( absint( $user_id ), 'colpt_research', wp_kses_post( $_POST['colpt_research'] ) );
	update_user_meta( absint( $user_id ), 'colpt_customtab1', wp_kses_post( $_POST['colpt_customtab1'] ) );
	update_user_meta( absint( $user_id ), 'colpt_member_type', wp_kses_post( $_POST['colpt_member_type'] ) );
	update_user_meta( absint( $user_id ), 'colpt_customtab2', wp_kses_post( $_POST['colpt_customtab2'] ) );
	update_user_meta( absint( $user_id ), 'colpt_customshortfield1', wp_kses_post( $_POST['colpt_customshortfield1'] ) );
	update_user_meta( absint( $user_id ), 'colpt_customshortfield2', wp_kses_post( $_POST['colpt_customshortfield2'] ) );
}

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );


function colpt_add_faculty_fields( $contact_methods ) {
	$contact_methods['colpt_title'] = 'Title';
	$contact_methods['colpt_phone'] = 'Phone';
	$contact_methods['colpt_office'] = 'Office Location';
	// $contact_methods['jabber'] = ''; // just removes label, have to find how to remove it.
	return $contact_methods;
};
add_filter( 'user_contactmethods', 'colpt_add_faculty_fields' );




//Kill Stuff on Profile Page (editing side)

function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
add_action('admin_head','hide_personal_options');

function hide_account_management_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'tr#password\').hide(); });</script>' . "\n";
}
add_action('admin_head','hide_account_management_options');

add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );

/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

        // remove the table row
        $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
        print $html;
    }
}










// Plugin Activation Functions





// Plugin Deactivation



function colpt_deactivation() {
 
    // Our post type will be automatically removed, so no need to unregister it
 
    // Clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
 
}

register_deactivation_hook( __FILE__, 'colpt_deactivation' );


// Shortcodes




?>