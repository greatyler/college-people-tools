<?php
/**
* Template Name: Faculty Profile from user profile
 *
 * @package WF College Two
 */

get_header(); ?>

<?php 
include(dirname(__FILE__) . '/pp_functions.php');
?>

 

 
 
 

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
			
			



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="edudms-entry-content">
	
	
	<?php
	
	if (get_option('colpt_pp_layout_piece_top_text_setting') == '1' ) {
		echo '<div class="pp_header_top_text">';
		echo wpautop(get_option('colpt_pp_top_text_editor'));
		echo '</div>';
	}
	
	
	
	if (get_option('colpt_pp_layout_piece_1_setting') == '1' ) {
		
		echo '<div class="label3">Faculty</div>';
		
	$args = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => '',
	'meta_key'     => 'colpt_member_type',
	'meta_value'   => 'faculty',
	'meta_compare' => '',
	'meta_query'   => array(),
	'date_query'   => array(),        
	'include'      => array(),
	'exclude'      => array(),
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => '',
 ); 

$colpt_person = get_users( $args );
	
	
	
	colpt_header_start();
	//Create Header Divs Here
	colpt_make_header('colpt_pp_block_1_selection');
	colpt_make_header('colpt_pp_block_2_selection');
	colpt_make_header('colpt_pp_block_3_selection');
	colpt_make_header('colpt_pp_block_4_selection');
	colpt_make_header('colpt_pp_block_5_selection');
	colpt_make_header('colpt_pp_block_6_selection');
	colpt_make_header('colpt_pp_block_7_selection');
	//No more Header Divs beyond here
	colpt_header_end();
	
	
usort($colpt_person, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));	
	foreach ( $colpt_person as $user ) {
		$user_identifier = $user->id;
		$profile_template_page = get_option('colpt_profile_page_selection');
		$profile_link = get_permalink( $profile_template_page ) . '?user=' . $user_identifier;
		$profile_blob = '<a href="' . $profile_link .  '">';
		$profile_blob2 = esc_html($profile_blob);
		$first_name = $user->first_name;
		$last_name = $user->last_name;
		$title = $user->colpt_title;
		$email = $user->user_email;
		$website = $user->user_url;
		$phone = $user->colpt_phone;
		$office = $user->colpt_office;
		$colpt_bio = $user->colpt_bio;
		$colpt_cv = $user->colpt_cv;
		$colpt_courses = $user->colpt_courses;
		$full_name = $first_name . ' ' . $last_name;
		$comma_name = $last_name . ', ' . $first_name;
		$colpt_customshortfield1 = $user->colpt_customshortfield1;
		$colpt_customshortfield2 = $user->colpt_customshortfield2;

		
	colpt_person_setup_block('block_1_value', 'block_1_data_type', 'colpt_pp_block_1_selection');
	colpt_person_setup_block('block_2_value', 'block_2_data_type', 'colpt_pp_block_2_selection');
	colpt_person_setup_block('block_3_value', 'block_3_data_type', 'colpt_pp_block_3_selection');
	colpt_person_setup_block('block_4_value', 'block_4_data_type', 'colpt_pp_block_4_selection');
	colpt_person_setup_block('block_5_value', 'block_5_data_type', 'colpt_pp_block_5_selection');
	colpt_person_setup_block('block_6_value', 'block_6_data_type', 'colpt_pp_block_6_selection');
	colpt_person_setup_block('block_7_value', 'block_7_data_type', 'colpt_pp_block_7_selection');
	colpt_person_start();
	colpt_wrap_it_start('colpt_pp_name_and_title_layout_selection');
	colpt_show_it($block_1_value, $block_1_data_type, 2 , get_option('colpt_pp_block_1_linkon_selection'));
	colpt_show_it($block_2_value, $block_2_data_type, 2 , get_option('colpt_pp_block_2_linkon_selection'));
	colpt_wrap_it_end('colpt_pp_name_and_title_layout_selection');
	colpt_show_it($block_3_value, $block_3_data_type, 2 , get_option('colpt_pp_block_3_linkon_selection'));
	colpt_show_it($block_4_value, $block_4_data_type, 2 , get_option('colpt_pp_block_4_linkon_selection'));
	colpt_show_it($block_5_value, $block_5_data_type, 2 , get_option('colpt_pp_block_5_linkon_selection'));
	colpt_show_it($block_6_value, $block_6_data_type, 2 , get_option('colpt_pp_block_6_linkon_selection'));
	colpt_show_it($block_7_value, $block_7_data_type, 2 , get_option('colpt_pp_block_7_linkon_selection'));
	colpt_person_end();
	}
	}
	?>


<?php
	
	
	if (get_option('colpt_pp_layout_piece_2_setting') == '1' ) {

		echo '<div class="label3">Staff</div>';

	
$args = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => '',
	'meta_key'     => 'colpt_member_type',
	'meta_value'   => 'staff',
	'meta_compare' => '',
	'meta_query'   => array(),
	'date_query'   => array(),        
	'include'      => array(),
	'exclude'      => array(),
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => '',
 ); 

$colpt_person = get_users( $args );
	
	
	colpt_header_start();
	//Create Header Divs Here
	colpt_make_header('colpt_pp_block_1_selection');
	colpt_make_header('colpt_pp_block_2_selection');
	colpt_make_header('colpt_pp_block_3_selection');
	colpt_make_header('colpt_pp_block_4_selection');
	colpt_make_header('colpt_pp_block_5_selection');
	colpt_make_header('colpt_pp_block_6_selection');
	colpt_make_header('colpt_pp_block_7_selection');
	//No more Header Divs beyond here
	colpt_header_end();
	
	
usort($colpt_person, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));	
	foreach ( $colpt_person as $user ) {
		$user_identifier = $user->id;
		$profile_template_page = get_option('colpt_profile_page_selection');
		$profile_link = get_permalink( $profile_template_page ) . '?user=' . $user_identifier;
		$profile_blob = '<a href="' . $profile_link .  '">';
		$profile_blob2 = esc_html($profile_blob);
		$first_name = $user->first_name;
		$last_name = $user->last_name;
		$title = $user->colpt_title;
		$email = $user->user_email;
		$website = $user->user_url;
		$phone = $user->colpt_phone;
		$office = $user->colpt_office;
		$colpt_bio = $user->colpt_bio;
		$colpt_cv = $user->colpt_cv;
		$colpt_courses = $user->colpt_courses;
		$full_name = $first_name . ' ' . $last_name;
		$comma_name = $last_name . ', ' . $first_name;
		$colpt_customshortfield1 = $user->colpt_customshortfield1;
		$colpt_customshortfield2 = $user->colpt_customshortfield2;

		
	colpt_person_setup_block('block_1_value', 'block_1_data_type', 'colpt_pp_block_1_selection');
	colpt_person_setup_block('block_2_value', 'block_2_data_type', 'colpt_pp_block_2_selection');
	colpt_person_setup_block('block_3_value', 'block_3_data_type', 'colpt_pp_block_3_selection');
	colpt_person_setup_block('block_4_value', 'block_4_data_type', 'colpt_pp_block_4_selection');
	colpt_person_setup_block('block_5_value', 'block_5_data_type', 'colpt_pp_block_5_selection');
	colpt_person_setup_block('block_6_value', 'block_6_data_type', 'colpt_pp_block_6_selection');
	colpt_person_setup_block('block_7_value', 'block_7_data_type', 'colpt_pp_block_7_selection');
	colpt_person_start();
	colpt_wrap_it_start('colpt_pp_name_and_title_layout_selection');
	colpt_show_it($block_1_value, $block_1_data_type, 2 , get_option('colpt_pp_block_1_linkon_selection'));
	colpt_show_it($block_2_value, $block_2_data_type, 2 , get_option('colpt_pp_block_2_linkon_selection'));
	colpt_wrap_it_end('colpt_pp_name_and_title_layout_selection');
	colpt_show_it($block_3_value, $block_3_data_type, 2 , get_option('colpt_pp_block_3_linkon_selection'));
	colpt_show_it($block_4_value, $block_4_data_type, 2 , get_option('colpt_pp_block_4_linkon_selection'));
	colpt_show_it($block_5_value, $block_5_data_type, 2 , get_option('colpt_pp_block_5_linkon_selection'));
	colpt_show_it($block_6_value, $block_6_data_type, 2 , get_option('colpt_pp_block_6_linkon_selection'));
	colpt_show_it($block_7_value, $block_7_data_type, 2 , get_option('colpt_pp_block_7_linkon_selection'));
	colpt_person_end();
	}
	}
	?>







	
	
	
	</div> <!-- End entry content div -->

</article><!-- #post-## -->


		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>