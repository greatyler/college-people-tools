<?php
//Test for WordPress Version
function cm_wp_version_check()
{
	global $wp_version;

	if ( !version_compare($wp_version,"4.0",">=") )
	{
		die("You need at least version 4.0 of WordPress to use this plugin");
	}
}

//Test for College Theme Version
function cm_theme_version_check()
{
	$my_theme = wp_get_theme();
	echo $my_theme->get( 'Name' ) . " is version " . $my_theme->get( 'Version' );
	if ( !version_compare($my_theme->get( 'Version' ),"1.7",">=") )
	{
		die("You need at least version 1.0 of the College Two theme to use this plugin");
	}
}



?>