<?php
/**
* Template Name: Faculty Profile from user profile
 *
 * @package WF College Two
 */

get_header(); ?>

<?php $colpt_person = get_users( 'include=' . $_GET['user'] );?>

<?php include(dirname(__FILE__) . '/pp_functions.php'); ?>





<?php foreach ( array_slice($colpt_person, 0, 1) as $user ) { 
	$useridentifier = $_GET["user"]; //receive user id from URL
	$first_name = $user->first_name;
	$last_name = $user->last_name;
	$title = $user->colpt_title; /* Get title field added to user profile */
	$email = $user->user_email;
	$website = $user->user_url;
	$phone = $user->colpt_phone; // get value of field added in functions.php
	$office = $user->colpt_office; // another field added
	$colpt_bio = $user->colpt_bio;
	$colpt_cv = $user->colpt_cv;
	$colpt_courses = $user->colpt_courses;
	$colpt_publications = $user->colpt_publications;
	$colpt_research = $user->colpt_research;
	$colpt_customtab1 = $user->colpt_customtab1;
	$colpt_customtab2 = $user->colpt_customtab2;
	$colpt_member_type = $user->colpt_member_type;
?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
			
			



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="edudms-entry-content">


	<div id="profArea">
		<div id="profName"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></div>
		
		<div id="profPicArea">
			<div id="profPicIndent">
				<?php echo get_wp_user_avatar( $email, 250 ); ?>
			</div>
		</div>
		<div id="colpt_prp_title"><?php echo esc_html( $title ); ?></div>
		<div class="colpt_prp_contact_wrapper">
			<div id="colpt_prp_contact_item_wrapper"><span class="label2">Office:</span> <span class="colpt_prp_value office"><?php echo esc_html( $office ); ?></span></div>
			<div id="colpt_prp_contact_item_wrapper"><span class="label2">Email:<span> <span class="colpt_prp_value email"><?php echo '<a href="mailto:' . esc_attr( $email ) . '" >' . esc_html( $email ) . '</a>'; ?></span></div>
			<div id="colpt_prp_contact_item_wrapper"><span class="label2">Phone:</span> <span class="colpt_prp_value phone"><?php echo esc_html( $phone ); ?></span></div>
			
			<div id="website">
				<?php
					if( $website ) {
					echo '<span class="label">Website:</span> <span class="profilefield website">' . '<a href="' . esc_url( $website ) . '">' . $website . '</a></span>'; }; 
				?>
			</div>

		</div> <!-- colpt_prp_contact_wrapper -->
	</div> <!-- end of profArea -->
	
	
	

<?php if(get_option('colpt_bio_field_setting') == '1' && !empty($colpt_bio)) {	
		echo do_shortcode('[tabby title="Bio"]');
		echo wpautop( do_shortcode( $colpt_bio ) );
		
	} ?>
<?php if(get_option('colpt_cv_field_setting') == '1' && !empty($colpt_cv)) {
		echo do_shortcode('[tabby title="CV"]');
		echo wpautop( do_shortcode( $colpt_cv ));
	} ?>
<?php if(get_option('colpt_courses_field_setting') == '1' && !empty($colpt_courses)) {
		echo do_shortcode('[tabby title="Courses"]');
		echo wpautop( do_shortcode( $colpt_courses ));
	} ?>
<?php if(get_option('colpt_publications_field_setting') == '1' && !empty($colpt_publications)) {
		echo do_shortcode('[tabby title="Publications"]'); 
		echo wpautop( do_shortcode( $colpt_publications ));
	} ?>
<?php if(get_option('colpt_research_field_setting') == '1' && !empty($colpt_research)) {	
		echo do_shortcode('[tabby title="Research"]');
		echo wpautop( do_shortcode( $colpt_research ));
	} ?>

<?php 
	$customtab1_name = get_option( 'colpt_customtab1_name_field_setting' );
	$customtab1_status = get_option( 'colpt_customtab1_field_setting' );
	if ( $customtab1_status == 1 && !empty($colpt_customtab1) ) {
		echo do_shortcode('[tabby title="' . $customtab1_name . '"]');
		echo wpautop( do_shortcode($colpt_customtab1 )); 
	} ?>
	
<?php 
	$customtab2_name = get_option( 'colpt_customtab2_name_field_setting' );
	$customtab2_status = get_option( 'colpt_customtab2_field_setting' );
	if ( $customtab2_status == 1  && !empty($colpt_customtab2) ) {
		echo do_shortcode('[tabby title="' . $customtab2_name . '"]');
		echo wpautop( do_shortcode($colpt_customtab2 )); 
	} ?>

<?php echo do_shortcode('[tabbyending]'); ?>	


<?php } ?>










</div> <!-- End entry content div -->





</article><!-- #post-## -->


		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>