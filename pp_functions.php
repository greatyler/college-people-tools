<?php


function colpt_header_start() {
	echo '<div class="pp-header-block"> <!--Start Header Block-->';
}

function colpt_header_end() {
	echo '</div> <!--End Header Block-->';
}




function colpt_make_header( $optionname = null, $mini = 'full', $optionornot = true, $manualname = null ) {
	
	if ( $optionornot == true ) {
	$optionvalue = get_option($optionname);
	switch( $optionvalue ) {
	case "1":
		$header_text = null;
		break;
	case "2":
		$header_text = 'Name';
		break;
	case "3":
		$header_text = 'Name';
		break;
	case "4":
		if (get_option('colpt_pp_name_and_title_layout_selection') == 3 && $mini == 'full') {
			$header_text = null;
		}
		else{
			$header_text = 'Title';
		}
		break;
	case "5":
		$header_text = 'Email';
		break;
	case "6":
		$header_text = 'Phone';
		break;
	case "7":
		$header_text = 'Office';
		break;
	case "8":
		$header_text = get_option('colpt_customshortfield1_name_field_setting');
		break;
	case "9":
		$header_text = get_option('colpt_customshortfield2_name_field_setting');
		break;
	default:
		$header_text = 'Default Header';
		break;
	} }
	if ( $optionornot == false ) {
		$header_text = $manualname;
	}
	if (is_null($header_text)) {
	}
	else {
		echo '<div class="colpt_pp_header">';
		echo $header_text;
		echo '</div>';
	}
}

function colpt_person_start() {
	echo '<div class="person-block"> <!--Start Person Block ' . $GLOBALS["user_identifier"] . '-->';
}

function colpt_person_end() {
	echo '</div> <!--End Person Block ' . $GLOBALS["user_identifier"] . '-->';
}


function colpt_person_setup_block( $blockvarname, $blockdatavar, $optionname = null ) {
	$optionvalue = get_option($optionname);
	switch( $optionvalue ) {
	case "1":
		$GLOBALS[$blockvarname] = null;
		$GLOBALS[$blockdatavar] = 1;
		break;
	case "2":
		$GLOBALS[$blockvarname] = $GLOBALS["full_name"];
		$GLOBALS[$blockdatavar] = 'name';
		break;
	case "3":
		$GLOBALS[$blockvarname] = $GLOBALS["comma_name"];
		$GLOBALS[$blockdatavar] = 'name';
		break;
	case "4":
		$GLOBALS[$blockvarname] = $GLOBALS["title"];
		$GLOBALS[$blockdatavar] = 'title';
		break;
	case "5":
		$GLOBALS[$blockvarname] = $GLOBALS["email"];
		$GLOBALS[$blockdatavar] = 'email';
		break;
	case "6":
		$GLOBALS[$blockvarname] = $GLOBALS["phone"];
		$GLOBALS[$blockdatavar] = 'phone';
		break;
	case "7":
		$GLOBALS[$blockvarname] = $GLOBALS["office"];
		$GLOBALS[$blockdatavar] = 'office';
		break;
	case "8":
		$GLOBALS[$blockvarname] = $GLOBALS["colpt_customshortfield1"];
		$GLOBALS[$blockdatavar] = get_option('colpt_customshortfield1_name_field_setting');
		break;
	case "9":
		$GLOBALS[$blockvarname] = $GLOBALS["colpt_customshortfield2"];
		$GLOBALS[$blockdatavar] = get_option('colpt_customshortfield2_name_field_setting');
		break;
	default:
		echo 'this is default';
		break;
	}
	return;
}






function colpt_wrap_it() {

}


function colpt_show_it( $it, $blockdatatype, $proportion = 1, $linkon = null ) {
	
	if (is_null($it)) {
	}
	else {
		echo '<div class="' . $blockdatatype . ' show_it prop-' . $proportion . '">';
			if ($blockdatatype == 'email') {
				echo '<a href="mailto:' . $GLOBALS["email"] . '">';
			}
			elseif ($blockdatatype == 'name') {
				echo '<a href="' . $GLOBALS["profile_link"] . '">';
			}
		echo $it;
		
			if ($blockdatatype == 'email') {
				echo '</a>';
			}
			elseif ($blockdatatype == 'name') {
				echo '</a>';
			}
		echo '</div>';
	}
}

function colpt_wrap_it_start($option = 3, $proportion = 2) {
	if(get_option($option) !== '2') {
		echo '<div class="wrap_it prop-' . $proportion . '">';
	}
	else {
	}
}

function colpt_wrap_it_end($option = 3) {
	if(get_option($option) !== '2') {
		echo '</div>';
	}
}
















?>